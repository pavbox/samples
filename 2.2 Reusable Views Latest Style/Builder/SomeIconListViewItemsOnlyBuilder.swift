//
//  SomeIconListViewItemsOnlyBuilder.swift
//
//  Created by Павел Маринченко on 3/18/19.
//  Copyright © 2019 Surf. All rights reserved.
//

import UIKit

// MARK: - .itemsOnly

final class SomeIconListViewItemsOnlyBuilder {

    // MARK: Properties

    private var onStackViewTap: IntBlock?

    private var items: [SomeViewModel] = []
    private var frame: CGRect = .zero
    private var maxVisibleItems = 4

    // MARK: Internal methods

    func set(frame: CGRect) -> SomeIconListViewItemsOnlyBuilder {
        self.frame = frame
        return self
    }

    func set(items: [SomeViewModel]) -> SomeIconListViewItemsOnlyBuilder {
        self.items = items
        return self
    }

    func set(onStackViewTap: IntBlock?) -> SomeIconListViewItemsOnlyBuilder {
        self.onStackViewTap = onStackViewTap
        return self
    }

    func set(maxVisibleItems: Int) -> Self {
        self.maxVisibleItems = maxVisibleItems
        return self
    }

    // MARK: Builder

    func build(with color: UIColor?) -> SomeIconListView {
        return SomeIconListViewBuilder()
            .set(frame: frame)
            .set(items: items)
            .set(maxVisibleItems: maxVisibleItems)
            .set(onStackViewTap: onStackViewTap)
            .build(color: color)
    }

}
