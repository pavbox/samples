//
//  SomeIconListViewBuilder.swift
//
//  Created by Павел Маринченко on 3/18/19.
//  Copyright © 2019 Surf. All rights reserved.
//

final class SomeIconListViewBuilder {

    // MARK: Properties

    // NOTE: tap on the item returns his index
    private var onStackViewTap: IntBlock?

    private var maxVisibleItems = 0
    private var frame: CGRect = .zero

    private var items: [SomeViewModel] = []
    private var customView: UIView?

    // MARK: Internal methods

    /// setup max visible items
    func set(maxVisibleItems: Int) -> SomeIconListViewBuilder {
        self.maxVisibleItems = maxVisibleItems
        return self
    }

    func set(frame: CGRect) -> SomeIconListViewBuilder {
        self.frame = frame
        return self
    }

    func set(items: [SomeViewModel]) -> SomeIconListViewBuilder {
        self.items = items
        return self
    }

    func set(customView: UIView?) -> SomeIconListViewBuilder {
        self.customView = customView
        return self
    }

    func set(onStackViewTap: IntBlock?) -> SomeIconListViewBuilder {
        self.onStackViewTap = onStackViewTap
        return self
    }

    // MARK: Builder

    func build(color: UIColor?) -> SomeIconListView {
        let view = SomeIconListView(frame: frame)
        view.onStackViewTap = onStackViewTap
        view.set(customView: customView)
        view.fill(items: items, maxVisibleItems: maxVisibleItems, color: color)
        return view
    }
}
