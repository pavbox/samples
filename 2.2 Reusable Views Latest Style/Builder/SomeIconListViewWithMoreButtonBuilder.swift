//
//  SomeIconListViewWithMoreButtonBuilder.swift
//
//  Created by Павел Маринченко on 3/18/19.
//  Copyright © 2019 Surf. All rights reserved.
//

import UIKit

// MARK: - .withMoreButton

final class SomeIconListViewWithMoreButtonBuilder {

    // MARK: Enums

    private enum Constants {
        static let defaultMaxVisibleItemsCount = 3
    }

    // MARK: Properties

    private var onStackViewTap: IntBlock?

    private var maxVisibleItems = Constants.defaultMaxVisibleItemsCount
    private var items: [SomeViewModel] = []
    private var frame: CGRect = .zero

    // NOTE: like "see more" button
    private var button: UIButton?

    // MARK: Internal methods

    func set(button: UIButton?) -> SomeIconListViewWithMoreButtonBuilder {
        self.button = button
        return self
    }

    /// setup max visible items
    func set(maxVisibleItems: Int) -> SomeIconListViewWithMoreButtonBuilder {
        self.maxVisibleItems = maxVisibleItems
        return self
    }

    func set(frame: CGRect) -> SomeIconListViewWithMoreButtonBuilder {
        self.frame = frame
        return self
    }

    func set(items: [SomeViewModel]) -> SomeIconListViewWithMoreButtonBuilder {
        self.items = items
        return self
    }

    func set(onStackViewTap: IntBlock?) -> SomeIconListViewWithMoreButtonBuilder {
        self.onStackViewTap = onStackViewTap
        return self
    }

    // MARK: Builder

    func build() -> SomeIconListView {
        return SomeIconListViewBuilder()
            .set(customView: button)
            .set(frame: frame)
            .set(items: items)
            .set(maxVisibleItems: maxVisibleItems)
            .set(onStackViewTap: onStackViewTap)
            .build(color: nil)
    }

}
