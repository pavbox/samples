//
//  SomeIconListView.swift
//
//  Created by Павел Маринченко on 13/03/2019.
//  Copyright © 2019 Surf. All rights reserved.
//

/// NOTE: just uistackview wrapper for items and optionally contains customView (in last position)

final class SomeIconListView: UIStackView {

    // MARK: - Properties

    var onStackViewTap: IntBlock?

    // MARK: - Private properties

    /// NOTE: this stackview knows about ViewModel's Type of Items
    private var models: [SomeViewModel] = []
    /// NOTE: optional can set custom view (with some events, etc.)
    private var customView: UIView?

    // MARK: - UIView

    override func awakeFromNib() {
        super.awakeFromNib()
        configureAppearance()
    }

    // MARK: - Internal methods

    func set(customView: UIView?) {
        self.customView = customView
    }

    /// NOTE: Setup loading state.
    func fillLoading(count: Int, color: UIColor) {
        arrangedSubviews.forEach { $0.removeFromSuperview() }

        for _ in 0...(count - 1) {
            let someIconView = SomeIconView()
            /// NOTE: setup loading state for each item
            someIconView.configureLoading(color: color)
            addArrangedSubview(someIconView)
        }
    }

    /// NOTE: fill stackview with models
    /// NOTE: and you can setup how much items to show (ex. only first 3 items)
    func fill(models: [SomeViewModel], maxVisibleItems: Int, color: UIColor?) {
        self.models = models
        self.arrangedSubviews.forEach { $0.removeFromSuperview() }

        for (index, item) in self.models.enumerated() {
            guard index < maxVisibleItems else {
                configureCustomView(for: self, with: customView)
                break
            }

            let someIconView = SomeIconView()

            if color == nil {
                someIconView.configure(appearance: .transparent, for: item)
            } else {
                someIconView.configure(appearance: .colored, for: item)
            }
            someIconView.setContentHuggingPriority(.required, for: .horizontal)

            someIconView.onTap = { [weak self] in
                /// NOTE: setup individual tap for item
                self?.onStackViewTap?(index)
            }

            self.addArrangedSubview(someIconView)
        }
    }
}

// MARK: - Private methods

private extension SomeIconListView {
    func configureAppearance() {
        backgroundColor = .clear
        clipsToBounds = true
    }

    // NOTE: custom view appears when amount of items is bigger than we want to show
    // NOTE: for example this view can be UILabel with text "+3" or "more"
    func configureCustomView(for stackView: UIStackView, with customView: UIView?) {
        guard let customView = customView else {
            return
        }

        self.addArrangedSubview(customView)
    }
}
