//
//  SimpleScreenRouterInput.swift
//
//  Created by Маринченко Павел on 18/06/2018.
//  Copyright © 2018 Surf. All rights reserved.
//

protocol SimpleScreenRouterInput {
    func openWebView(with urlString: String)
    func openSafari(with urlString: String)
    func close()
}
