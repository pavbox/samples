//
//  SimpleScreenRouter.swift
//
//  Created by Маринченко Павел on 18/06/2018.
//  Copyright © 2018 Surf. All rights reserved.
//

import UIKit

final class SimpleScreenRouter {

    weak var view: ModuleTransitionable?

}

// MARK: - SimpleScreenRouterInput

extension SimpleScreenRouter: SimpleScreenRouterInput {

    func openWebView(with urlString: String) {
        if let url = URL(string: urlString) {
            let webView = ConfigurableSFSafariViewController(url: url)
            view?.presentModule(webView, animated: true, completion: nil)
        }
    }

    func openSafari(with urlString: String) {
        if let url = URL(string: urlString), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }

    func close() {
        view?.pop(animated: true)
    }

}
