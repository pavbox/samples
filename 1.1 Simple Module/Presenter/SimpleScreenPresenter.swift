//
//  SimpleScreenPresenter.swift
//
//  Created by Маринченко Павел on 18/06/2018.
//  Copyright © 2018 Surf. All rights reserved.
//

import Alamofire

final class SimpleScreenPresenter {

    // MARK: - Enums

    enum LinkTypes {
        case shared
        case privacy
    }

    // MARK: - Properties

    weak var view: SimpleScreenViewInput?
    var router: SimpleScreenRouterInput?

}

// MARK: - SimpleScreenViewOutput

extension SimplecreenPresenter: SimpleScreenViewOutput {

    func viewLoaded() {
        view?.setupInitialState(appVersion: ApplicationInfo.appVersion())
    }

    func openLink(for linkType: LinkTypes) {

        guard isNetworkAvailable else {
            SnackMessenger.shared.show(message: L10n.noInternetConnection)
            return
        }

        SnackMessenger.shared.hide()

        switch linkType {
        case .shared:
            router?.openSafari(with: URLConstants.sharedUrl)
        case .privacy:
            router?.openWebView(with: URLConstants.privateUrl)
        }
    }

    func closeView() {
        router?.close()
    }
}

// MARK: - Private methods

private extension SimpleScreenPresenter {

    var isNetworkAvailable: Bool {
        if let manager = NetworkReachabilityManager() {
            return manager.isReachable
        }

        return false
    }

}
