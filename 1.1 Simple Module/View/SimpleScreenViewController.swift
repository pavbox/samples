//
//  SimpleScreenViewController.swift
//
//  Created by Маринченко Павел on 18/06/2018.
//  Copyright © 2018 Surf. All rights reserved.
//

import UIKit

// ModuleTransitionable – protocol thats impl applicaiton routes

final class SimpleScreenViewController: UIViewController, ModuleTransitionable {

    // MARK: - IBOutlets

    // NOTE: xib removed
    @IBOutlet private weak var backgroundImageView: UIImageView!
    @IBOutlet private weak var someImageView: UIImageView!
    @IBOutlet private weak var mainTextLabel: UILabel!
    @IBOutlet private weak var versionLabel: UILabel!

    @IBOutlet private weak var sharedLinkButton: UIButton!
    @IBOutlet private weak var privacyLinkButton: UIButton!

    // MARK: - Properties

    var output: SimpleScreenViewOutput?

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        output?.viewLoaded()
    }

}

// MARK: - IBActions

private extension SimpleScreenViewController {

    @IBAction func sharedLinkTapped(_ sender: UIButton) {
        output?.openLink(for: .shared)
    }

    @IBAction func privacyLinkTapped(_ sender: UIButton) {
        output?.openLink(for: .privacy)
    }

}

// MARK: - SimpleScreenViewInput

extension SimpleScreenViewController: SimpleScreenViewInput {

    func setupInitialState(appVersion: String?) {
        configureSuperview()
        configureNavController()
        configureMainText()
        configureButtons()
        configureVersion(version: appVersion)
    }

}

// MARK: - Private Methods

private extension SimpleScreenViewController {

    func configureSuperview() {
        backgroundImageView.image = UIImage(asset: Asset.backgroundImage)
        backgroundImageView.contentMode = .scaleAspectFill
        someImageView.image = UIImage(asset: Asset.someImage)
        someImageView.contentMode = .scaleAspectFit
    }

    func configureNavController() {
        addBackBarButton(selector: #selector(self.close))
    }

    func configureMainText() {
        mainTextLabel.text = L10n.Simplescreen.mainText
        mainTextLabel.font = UIFont.systemFont(ofSize: 12, weight: .regular)
        mainTextLabel.textColor = ColorName.middleGrayTextColor.color
        mainTextLabel.textAlignment = .left
        mainTextLabel.numberOfLines = 0
    }

    func configureButtons() {
        baseLinkStyling(for: sharedLinkButton, with: L10n.Simplescreen.Links.shared)
        baseLinkStyling(for: privacyLinkButton, with: L10n.Simplescreen.Links.privacy)
    }

    func baseLinkStyling(for button: UIButton, with text: String) {
        let attrs: [ NSAttributedStringKey: Any ] = [
            .font: UIFont.systemFont(ofSize: 12, weight: .regular),
            .foregroundColor: ColorName.darkBlueTheme.color,
            .underlineStyle: 1
        ]

        let attributedString = NSAttributedString(string: text, attributes: attrs)
        button.setAttributedTitle(attributedString, for: UIControlState())
    }

    func configureVersion(version: String?) {
        if let version = version {
            versionLabel.font = UIFont.systemFont(ofSize: 13, weight: .heavy)
            versionLabel.textColor = ColorName.mainTheme.color
            versionLabel.textAlignment = .center
            versionLabel.text = L10n.Simpleapp.version(version)
        } else {
            versionLabel.text = nil
        }
    }

    @objc
    func close() {
        output?.closeView()
    }

}
