//
//  SimpleScreenViewInput.swift
//
//  Created by Маринченко Павел on 18/06/2018.
//  Copyright © 2018 Surf. All rights reserved.
//

protocol SimpleScreenViewInput: class {
    /// Method for setup initial state of view
    func setupInitialState(appVersion: String?)
}
