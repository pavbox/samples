//
//  SimpleScreenViewOutput.swift
//
//  Created by Маринченко Павел on 18/06/2018.
//  Copyright © 2018 Surf. All rights reserved.
//

protocol SimpleScreenViewOutput {
    /// Notify presenter that view is ready
    func viewLoaded()

    /// Open link by linkType
    func openLink(for linkType: SimpleScreenPresenter.LinkTypes)

    /// Pop module from nav controller stack
    func closeView()
}
