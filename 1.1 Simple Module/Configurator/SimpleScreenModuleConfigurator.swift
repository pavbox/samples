//
//  SimpleScreenModuleConfigurator.swift
//
//  Created by Маринченко Павел on 18/06/2018.
//  Copyright © 2018 Surf. All rights reserved.
//
//  Comment about role of module in application

final class SimpleScreenModuleConfigurator {

    func configure() -> SimpleScreenViewController {
        let view = SimpleScreenViewController()
        let presenter = SimpleScreenPresenter()
        let router = SimpleScreenRouter()

        presenter.view = view
        presenter.router = router
        router.view = view
        view.output = presenter

        return view
    }

}
