//
//  SwitchableTableViewCell.swift
//
//  Created by Маринченко Павел on 03/05/2018.
//  Copyright © 2018 Surf. All rights reserved.
//

import UIKit

// NOTE: Cell that can switch interface designs based on app state. Rare UI Component.
// NOTE: One of this – cell with question mark button (that calls popup text subview)

typealias SwitchableCellCompletion = (Bool) -> Void

enum SwitchableTableViewCellState: Int {
    case normal
    case loading
    case disabled
}

protocol SwitchableTableViewCellInput: class {

    /// Configures switchable cell with title and popup text
    /// - Parameters:
    ///     - title: main title text
    ///     - indexPath: place of current cell for identificate this in presenter/adapter
    ///     - switcher: current state of switcher
    ///     - info: text that contains in popup hint
    func configure(title: String?, at indexPath: IndexPath, switcher enabled: Bool, info: String?)

    /// Setup state of cell
    func setState(_ state: SwitchableTableViewCellState)

    /// Configure separator view
    func setSeparatorState(visible: Bool)

    /// Setups new state for questionMark button
    func setQuestionMarkState(active: Bool)

    /// Setups new value for switcher
    func setSwitcherValue(on isOn: Bool, animated: Bool)

    /// Request updates for cell (in mind: while we need to update cell without reloading tableView)
    func update()

}

// NOTE: Optional Output (Delegate) methods
@objc protocol SwitchableTableViewCellOutput: class {
    /// Notify about changed switcher value
    func didSwitcherValueChanged(index: IndexPath, enable: Bool, _ completion: @escaping SwitchableCellCompletion)

    /// Notify about tapped of questionMark button & intension to show popup of current cell
    @objc
    optional func didQuestionMarkTapped(show: Bool, text: String, from rect: CGRect)

    /// Method that send request for update without updating whole tablecells (just current cell)
    @objc
    optional func easyUpdate(_ completion: @escaping SwitchableCellCompletion)
}

final class SwitchableTableViewCell: UITableViewCell {

    // MARK: - Enums

    enum questionMarkBehaviorType {
        case always
        // NOTE: visible only on disable state
        case onlyDisabledState
        case onlyNormalState
        case exceptNormalAndActiveState
        case never
    }

    // MARK: - Constants

    private struct Constants {
        static let popupTopOffset: CGFloat = 6
        static let disabledStateAlpha: CGFloat = 0.7
        static let normalStateAlpha: CGFloat = 1.0
        static let animationDuration: TimeInterval = 1.8
        static let animationDelay: TimeInterval = 0.2
    }

    // MARK: - IBOutlets

    // NOTE: xib removed
    @IBOutlet private weak var titleLabel: UILabel!
    // NOTE: have mark button
    @IBOutlet private weak var questionMarkButton: UIButton!
    // NOTE: have uiswitch
    @IBOutlet private weak var switcher: UISwitch!
    @IBOutlet private weak var separatorView: UIView!
    @IBOutlet private weak var shimmerView: UIView!

    // MARK: - Public Properties

    weak var output: SwitchableTableViewCellOutput?

    /// Configures visibility behavior of questionMark button
    /// If QuestionMark is visible, you can to implement optional easyUpdate method in your delegate
    var questionMarkVisibility: questionMarkBehaviorType = .never {
        didSet {
            updateUIState()
        }
    }

    // MARK: - Private Properties

    /// Contains state of cell and update UI if changes
    private var state: SwitchableTableViewCellState = .normal
    /// Contains enabled/disabled switcher value and update UI colors after changes
    private var switcherValue = false
    /// Contains information text for paste into popup hint
    private var infoText = ""
    /// Index is key for searching current model of cell in presenter/adapter that implement delegate
    /// NOTE: we need to know number of cell in tableview
    private var indexPath = IndexPath(item: 0, section: 0)

    /// Contains active/deactive state of questionMark and update UI colors after changes
    private var questionMarkIsActive = false {
        didSet {
            questionMarkButton.backgroundColor = (questionMarkIsActive)
                ? ColorName.darkGrayColor.color
                : ColorName.textLightGray.color
        }
    }

    /// NOTE: shimmer's animation helpers
    private var shouldStopAnimating = false
    private var animationAlreadyRunning = false

    // MARK: - Lifecycle

    override func awakeFromNib() {
        super.awakeFromNib()
        prepareUIPresentation()
    }

}

// MARK: - IBActions

private extension SwitchableTableViewCell {

    /// tap on questionMarkButton
    @IBAction func questionMarkTapped(_ sender: UIButton) {
        if !questionMarkIsActive {
            // gets absolute position of Point in this cell
            // NOTE: that needs for draw and show popup view with text in correct place of cell
            let xOffset = self.frame.origin.x + questionMarkButton.frame.minX + questionMarkButton.frame.width / 2
            let yOffset = self.frame.origin.y + questionMarkButton.frame.minY + questionMarkButton.frame.height + Constants.popupTopOffset
            let rect = CGRect(x: xOffset, y: yOffset, width: 0, height: 0)

            output?.didQuestionMarkTapped?(show: true, text: infoText, from: rect)
        } else {
            output?.didQuestionMarkTapped?(show: false, text: "", from: CGRect.zero)
        }

        self.questionMarkIsActive = !self.questionMarkIsActive
    }

    @IBAction func didSwitcherTapped(_ sender: UISwitch) {
        // block second value changed after setup state programmaticaly
        if switcherValue != sender.isOn {
            // NOTE: Change cell value in parent ViewModel by delegate call
            output?.didSwitcherValueChanged(index: indexPath, enable: sender.isOn) { (newSwitcherState) in
                DispatchQueue.main.async {
                    if self.switcherValue != newSwitcherState {
                        self.switcherValue = newSwitcherState
                        self.updateQuestionMarkVisibility()
                    }

                    sender.setOn(self.switcherValue, animated: true)
                }
            }
        }
    }

}

// MARK: - SwitchableTableViewCellInput

extension SwitchableTableViewCell: SwitchableTableViewCellInput {

    /// NOTE: configure this cell
    func configure(title: String?, at indexPath: IndexPath, switcher enabled: Bool = false, info: String? = nil) {
        self.titleLabel.text = title
        self.indexPath = indexPath
        self.infoText = info ?? ""

        switcher.setOn(enabled, animated: false)
        switcherValue = enabled
    }

    func setState(_ state: SwitchableTableViewCellState) {
        self.state = state
        updateUIState()
    }

    func setSeparatorState(visible: Bool) {
        separatorView.isHidden = !visible
    }

    func setQuestionMarkState(active: Bool) {
        questionMarkIsActive = active
    }

    func setSwitcherValue(on isOn: Bool, animated: Bool) {
        switcherValue = isOn
        switcher.setOn(isOn, animated: animated)
    }

    /// NOTE: Optinal logic. Cell automatically make async request data for switcher.
    /// NOTE: Async update allow to you skip screen loading state (loading state only for cells)
    /// NOTE: Thats useful in situations when value of switcher may be changed by Server.
    func update() {
        setState(.loading)
        output?.easyUpdate?({ [weak self] (enabled) in
            self?.switcherValue = enabled
            self?.setState(enabled ? .normal : .disabled)
            self?.switcher.setOn(enabled, animated: true)
        })
    }

}

// MARK: - Configure

private extension SwitchableTableViewCell {

    func prepareUIPresentation() {
        prepareTitleStyles()
        prepareSwitcherStyles()
        prepareSeparatorView()

        prepareQuestionMarkStyles()
        prepareLoaderState()
        updateUIState()
    }

    func prepareTitleStyles() {
        titleLabel.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        titleLabel.textColor = ColorName.darkBlueTheme.color
    }

    func prepareQuestionMarkStyles() {
        questionMarkButton.setTitleColor(ColorName.mainTheme.color, for: .normal)
        questionMarkButton.backgroundColor = ColorName.textLightGray.color
        questionMarkButton.layer.cornerRadius = questionMarkButton.frame.height / 2
    }

    func prepareSwitcherStyles() {
        switcher.applyDefaultStyle()
    }

    func prepareSeparatorView() {
        separatorView.backgroundColor = ColorName.lightGray.color
    }

    func prepareLoaderState() {
        createGradientLayer()
    }
}

// MARK: - States of cell

private extension SwitchableTableViewCell {

    func updateUIState() {
        switch state {
        case .normal:
            setNormalState()
        case .loading:
            setLoadingState()
        case .disabled:
            setDisabledState()
        }
    }

    func setNormalState() {
        titleLabel.textColor = ColorName.darkBlueTheme.color
        questionMarkButton.alpha = Constants.normalStateAlpha
        switcher.alpha = Constants.normalStateAlpha
        stopLoading()

        questionMarkButton.isUserInteractionEnabled = true
        titleLabel.isUserInteractionEnabled = true
        switcher.isUserInteractionEnabled = true
        updateQuestionMarkVisibility()
    }

    func setLoadingState() {
        titleLabel.textColor = ColorName.darkBlueTheme.color
        questionMarkButton.alpha = Constants.normalStateAlpha
        switcher.alpha = Constants.normalStateAlpha
        startLoading()

        questionMarkButton.isUserInteractionEnabled = false
        titleLabel.isUserInteractionEnabled = false
        switcher.isUserInteractionEnabled = false
        updateQuestionMarkVisibility()
    }

    func setDisabledState() {
        titleLabel.textColor = ColorName.textLightGray.color
        questionMarkButton.alpha = Constants.disabledStateAlpha
        switcher.alpha = Constants.disabledStateAlpha
        stopLoading()

        questionMarkButton.isUserInteractionEnabled = true
        titleLabel.isUserInteractionEnabled = false
        switcher.isUserInteractionEnabled = false
        updateQuestionMarkVisibility()
    }

    /// Controls questionMark visibility
    func updateQuestionMarkVisibility() {
        switch questionMarkVisibility {
        case .always:
            questionMarkButton.isHidden = false
        case .onlyNormalState:
            questionMarkButton.isHidden = (state != .normal)
        case .exceptNormalAndActiveState:
            questionMarkButton.isHidden = (state == .normal && switcherValue)
        case .onlyDisabledState:
            questionMarkButton.isHidden = (state != .disabled)
        case .never:
            questionMarkButton.isHidden = true
        }
    }

}

// MARK: - Animation Handling

private extension SwitchableTableViewCell {

    func startLoading() {
        if animationAlreadyRunning {
            return
        }

        shimmerView.isHidden = false

        if !animationAlreadyRunning {
            animationAlreadyRunning = true
            shouldStopAnimating = false
            startLoaderAnimation()
        }
    }

    func stopLoading() {
        shimmerView.isHidden = true
        shouldStopAnimating = true
        animationAlreadyRunning = false
    }

    func createGradientLayer() {
        let gradient = CAGradientLayer()
        gradient.frame = shimmerView.bounds
        gradient.colors = [
            UIColor.white.withAlphaComponent(0).cgColor,
            UIColor.white.withAlphaComponent(0.8).cgColor,
            UIColor.white.withAlphaComponent(0).cgColor
        ]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)

        shimmerView.layer.addSublayer(gradient)
        shimmerView.isHidden = true
    }

}

// MARK: - Animation

private extension SwitchableTableViewCell {

    func startLoaderAnimation() {
        animate(shimmer: shimmerView)
    }

    // NOTE: Not best solution
    // NOTE: recursive animation repeating (without leaks)
    // NOTE: just moving shimmer layer from left to right
    func animate(shimmer: UIView) {
        shimmer.frame.origin = CGPoint(x: -shimmer.frame.width, y: 0)
        UIView.animate(withDuration: Constants.animationDuration, delay: Constants.animationDelay, options: .curveLinear, animations: {
            shimmer.frame.origin = CGPoint(x: self.frame.maxX, y: 0)
        }) { (_) in
            if (!self.shouldStopAnimating) {
                self.animate(shimmer: shimmer)
            } else {
                self.shouldStopAnimating = true
                return
            }
        }
    }

}
