//
//  AnotherSimpleScreenViewOutput.swift
//
//  Created by Маринченко Павел on 06/07/2018.
//  Copyright © 2018 Surf. All rights reserved.
//

protocol AnotherSimpleScreenViewOutput {
    /// Notify presenter that view is ready
    func viewLoaded()
    func openSomeList()
    func closeTap()
    func returnTap()
}
