//
//  AnotherSimpleScreenViewController.swift
//
//  Created by Маринченко Павел on 06/07/2018.
//  Copyright © 2018 Surf. All rights reserved.
//

import UIKit

final class AnotherSimpleScreenViewController: UIViewController, ModuleTransitionable {

    // MARK: - Constants

    private struct Constants {
        static let closeButtonLeftOffsetDefault: CGFloat = 10
        static let imageViewTopOffsetDefault: CGFloat = 80
        static let imageViewBetweenMessageOffsetDefault: CGFloat = 16
        static let bottomButtonsOffsetDefault: CGFloat = 35

        static let closeButtonLeftOffsetIPhoneX: CGFloat = 16
        static let imageViewTopOffsetIPhoneX: CGFloat = 100
        static let imageViewBetweenMessageOffsetIPhoneX: CGFloat = 24
        static let bottomButtonsOffsetIPhoneX: CGFloat = 80
    }

    // MARK: - IBOutlets

    @IBOutlet private weak var closeButton: UIButton!
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var messageLabel: UILabel!
    @IBOutlet private weak var returnButton: UIButton!
    @IBOutlet private weak var openSomeListButton: UIButton!

    // MARK: - Constraints

    @IBOutlet private weak var closeButtonLeftOffsetConstraint: NSLayoutConstraint!
    @IBOutlet private weak var imageViewTopOffsetConstraint: NSLayoutConstraint!
    @IBOutlet private weak var imageViewBetweenMessageConstraint: NSLayoutConstraint!
    @IBOutlet private weak var bottomButtonsOffsetConstraint: NSLayoutConstraint!

    // MARK: - Properties

    var output: AnotherSimpleScreenViewOutput?

    // MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()
        output?.viewLoaded()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }

}

// MARK: - IBActions

private extension AnotherSimpleScreenViewController {
    @IBAction func openSomeListTapped(_ sender: UIButton) {
        output?.openSomeList()
    }

    @IBAction func closeButtonTapped(_ sender: UIButton) {
        output?.closeTap()
    }

    @IBAction func returnButtonTapped(_ sender: UIButton) {
        output?.returnTap()
    }
}

// MARK: - AnotherSimpleScreenViewInput

extension AnotherSimpleScreenViewController: AnotherSimpleScreenViewInput {
    func setupInitialState() {
        configureCloseButton()
        configureImageView()
        configureMessageLabel()
        configureReturnButton()
        configureOpenSomeListButton()
        configureConstraintsIfNeeded()
    }
}

// MARK: - Private methods

private extension AnotherSimpleScreenViewController {
    func configureCloseButton() {
        closeButton.setImage(UIImage(asset: Asset.NavigationBar.close), for: .normal)
        closeButton.contentMode = .center
        closeButton.tintColor = ColorName.darkBlueTheme.color
    }

    func configureImageView() {
        imageView.image = UIImage(asset: Asset.exclamationMark)
        imageView.contentMode = .scaleAspectFit
    }

    func configureMessageLabel() {
        messageLabel.setAttributedText(text: L10n.Anothersimplescreen.message, lineSpacing: 4.0, letterSpacing: nil)
        messageLabel.textColor = ColorName.darkGrayColor.color
        messageLabel.textAlignment = .center
    }

    func configureReturnButton() {
        returnButton.layer.cornerRadius = returnButton.frame.height / 2
        returnButton.backgroundColor = ColorName.mainGreenTheme.color

        returnButton.setTitle(L10n.Anothersimplescreen.Button.back, for: .normal)
        returnButton.setTitleColor(ColorName.mainTheme.color, for: .normal)
        returnButton.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .medium)
        returnButton.titleLabel?.textAlignment = .center
    }

    func configureOpenSomeListButton() {
        openSomeListButton.setTitle(L10n.Anothersimplescreen.Button.openOfficeList, for: .normal)
        openSomeListButton.setTitleColor(ColorName.mainGreenTheme.color, for: .normal)
        openSomeListButton.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .medium)
        openSomeListButton.titleLabel?.textAlignment = .center
    }

    // NOTE: rare design solution needs to be adaptive with this hardcode
    func configureConstraintsIfNeeded() {
        if UIDevice.current.isXVersion {
            closeButtonLeftOffsetConstraint.constant = Constants.closeButtonLeftOffsetIPhoneX
            imageViewTopOffsetConstraint.constant = Constants.imageViewTopOffsetIPhoneX
            imageViewBetweenMessageConstraint.constant = Constants.imageViewBetweenMessageOffsetIPhoneX
            bottomButtonsOffsetConstraint.constant = Constants.bottomButtonsOffsetIPhoneX
        } else {
            closeButtonLeftOffsetConstraint.constant = Constants.closeButtonLeftOffsetDefault
            imageViewTopOffsetConstraint.constant = Constants.imageViewTopOffsetDefault
            imageViewBetweenMessageConstraint.constant = Constants.imageViewBetweenMessageOffsetDefault
            bottomButtonsOffsetConstraint.constant = Constants.bottomButtonsOffsetDefault
        }
    }
}
