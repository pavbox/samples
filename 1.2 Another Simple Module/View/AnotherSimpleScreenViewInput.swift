//
//  AnotherSimpleScreenViewInput.swift
//
//  Created by Маринченко Павел on 06/07/2018.
//  Copyright © 2018 Surf. All rights reserved.
//

protocol AnotherSimpleScreenViewInput: class {
    /// Method for setup initial state of view
    func setupInitialState()
}
