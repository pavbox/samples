//
//  AnotherSimpleScreenModuleConfigurator.swift
//
//  Created by Маринченко Павел on 06/07/2018.
//  Copyright © 2018 Surf. All rights reserved.
//
//  Comment describes reusable modal module...

final class AnotherSimpleScreenModuleConfigurator {

    func configure(didCloseTap: @escaping EmptyBlock) -> AnotherSimpleScreenViewController {
        let view = AnotherSimpleScreenViewController()
        let presenter = AnotherSimpleScreenPresenter(closeBlock: didCloseTap)
        let router = AnotherSimpleScreenRouter()

        presenter.view = view
        presenter.router = router
        router.view = view
        view.output = presenter

        return view
    }

}
