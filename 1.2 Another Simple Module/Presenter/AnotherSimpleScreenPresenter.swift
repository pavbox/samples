//
//  AnotherSimpleScreenPresenter.swift
//
//  Created by Маринченко Павел on 06/07/2018.
//  Copyright © 2018 Surf. All rights reserved.
//

final class AnotherSimpleScreenPresenter {

    // MARK: - Properties

    weak var view: AnotherSimpleScreenViewInput?
    var router: AnotherSimpleScreenRouterInput?

    private var closeBlock: EmptyBlock?

    // MARK: - Initialization and deinitialization

    init(closeBlock: EmptyBlock?) {
        self.closeBlock = closeBlock
    }

}

// MARK: - AnotherSimpleScreenViewOutput

extension AnotherSimpleScreenPresenter: AnotherSimpleScreenViewOutput {

    func viewLoaded() {
        view?.setupInitialState()
    }

    func openSomeList() {
        router?.openWebView(with: URLConstants.someListUrl)
    }

    func closeTap() {
        closeBlock?()
        router?.close()
    }

    func returnTap() {
        closeTap()
    }
}
