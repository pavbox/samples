//
//  AnotherSimpleScreenRouter.swift
//
//  Created by Маринченко Павел on 06/07/2018.
//  Copyright © 2018 Surf. All rights reserved.
//

import UIKit

final class AnotherSimpleScreenRouter {

    weak var view: ModuleTransitionable?

}

// MARK: - AnotherSimpleScreenRouterInput

extension AnotherSimpleScreenRouter: AnotherSimpleScreenRouterInput {

    func openWebView(with url: String) {
        if let url = URL(string: url) {
            let webView = ConfigurableSFSafariViewController(url: url)
            webView.modalPresentationCapturesStatusBarAppearance = true
            view?.presentModule(webView, animated: true, completion: nil)
        }
    }

    func close() {
        view?.dismissView(animated: true, completion: nil)
    }

}
