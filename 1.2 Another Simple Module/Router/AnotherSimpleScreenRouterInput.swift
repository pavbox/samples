//
//  AnotherSimpleScreenRouterInput.swift
//  ZenitOnline
//
//  Created by Маринченко Павел on 06/07/2018.
//  Copyright © 2018 Surf. All rights reserved.
//

protocol AnotherSimpleScreenRouterInput {
    func openWebView(with url: String)
    func close()
}
